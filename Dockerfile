# syntax=docker/dockerfile:1
ARG RUBY_VERSION=3.1.2

FROM ruby:${RUBY_VERSION} as builder
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
WORKDIR /app
COPY Gemfile .
COPY Gemfile.lock .
RUN bundle install

FROM ruby:${RUBY_VERSION}
WORKDIR /app

COPY --from=builder /usr/local/bundle/ /usr/local/bundle/

# Copy app code
COPY . .

# Create and switch to app user
RUN useradd -m app
USER app

EXPOSE 3000

# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]

