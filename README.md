# Simple Monitoring Solution

This project demonstrates how to use the [prometheus-tiny](https://gitlab.com/toanvvsg/prometheus-tiny) gem, a simple and easy-to-use prometheus exporter for ruby on rails applications. The project relies on these services:

- db: a postgresql database to store and manage the application data.
- app: a simple ruby on rails application included with `prometheus-tiny` to expose prometheus metrics.
- dashboard: Grafana a visualization and analytics platform to create and display dashboards and metrics from various data sources.
- prometheus: a monitoring and alerting system that collects and stores metrics from the web application and other services.
- traffic-maker: use curl to generate simulated traffic for testing monitoring solution.

![plot](./images/demo.jpg)

## Prerequisites

To run this project, you need to have Docker and Docker Compose installed on your machine. You can follow the instructions here to install them:

- [Install Docker](https://docs.docker.com/get-docker/)

## How to run

To run this project, you need to use the `docker compose` command with the provided `docker-compose.yml` file. For example, you can use the following command to start all the services in the background:

```shell
cp .env.sample .env
docker compose up -d
```

## How to access

Once all the services are running, you can access them using the following URLs:

- Web application: http://localhost:3000
- Metrics: http://localhost:3000/metrics
- Grafana: http://localhost:3090
- Prometheus: http://localhost:9090

You can also use `docker compose ps` command to check the status of each service, or use `docker compose logs` command to view the logs of each service.

Default username and password for grafana is admin/admin

## How to customize

You can customize this project by modifying the files in the following directories:

- `./app`: The source code and configuration files for the Rails application. You can use the [prometheus-tiny](https://gitlab.com/toanvvsg/prometheus-tiny) gem to define and expose metrics from your application logic. For more information on how to use the gem, please refer to its [documentation](https://gitlab.com/toanvvsg/prometheus-tiny/-/blob/master/README.md).
- `./prometheus.yml`: The configuration file for Prometheus, where you can define what metrics to collect and how often.
- `./grafana/provisioning`: The configuration files for Grafana, where you can define what data sources and dashboards to use.
